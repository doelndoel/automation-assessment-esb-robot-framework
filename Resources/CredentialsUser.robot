*** Variables ***
${SiteUrl}            https://www.saucedemo.com/
${Username}           standard_user
${Password}           secret_sauce
${WrongPassword}      wrong password
${WrongUsername}      wrong_username
${FirstName}          Abdul
${LastName}           Rozzaaq
${PostalCode}         57692