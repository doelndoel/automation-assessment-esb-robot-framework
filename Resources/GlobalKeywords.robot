*** Settings ***
Library    SeleniumLibrary
Library    Collections
Resource   GlobalVariable.robot
Resource   CredentialsUser.robot
*** Keywords ***
Open Site
    open browser       ${SiteUrl}    ${Browser}
Input Credential
    Input Text    id:user-name                                    ${Username}
    Input Password    id:password                                 ${Password}
Input Wrong Password Credential
    Input Text        id:user-name                                ${Username}
    Input Password    id:password                                 ${WrongPassword}
Input Wrong Username Credential
    Input Text        id:user-name                                ${WrongUsername}
    Input Password    id:password                                 ${Password}
Click Button Login
    Click Element    id:login-button
Wait Element Image
    Wait Until Element Is Visible   id:item_0_img_link
Check Dashboard Title
    Wait Until Element Contains    xpath:/html/body/div/div/div/div[1]/div[1]/div[2]/div  ${AppTitle}
Check Title Product
    Wait Until Element Contains    xpath:/html/body/div/div/div/div[1]/div[2]/span  ${Title}
Check List Item
    Wait Until Element Is Visible   id:inventory_container
Click Add to cart
    Click Button    id:add-to-cart-sauce-labs-backpack
Click Icon Cart
    Wait Until Element Is Visible   id:shopping_cart_container
    ${visible}  Run Keyword And Return Status
    ...         Wait Until Element Is Enabled    xpath:/html/body/div/div/div/div[1]/div[1]/div[3]/a/span
    IF  ${visible} == True
        ${counting}    Get Text         xpath:/html/body/div/div/div/div[1]/div[1]/div[3]/a/span
        IF    ${counting} != 0
            Click Element        id:shopping_cart_container    
        ELSE
           Fail      Can't click icon !!
        END        
    ELSE
        Fail      Please Add to Cart !!
        Close Browser
    END
Cart Page Detail
    Wait Until Element Contains    xpath:/html/body/div/div/div/div[1]/div[2]/span    ${TitleCartPage}
    ${cartPage}  Run Keyword And Return Status
    ...                Wait Until Element Is Enabled         xpath:/html/body/div/div/div/div[2]/div/div[1]
    
    IF   ${cartPage} == True
        Click Button    id:checkout
    ELSE
        Click Button    id:continue-shopping
        Fail        Cart List Empty 
        Close Browser
    END
Fill Detail Information
    ${TitleCartPage}  Run Keyword And Return Status
    ...                Wait Until Element Contains    xpath:/html/body/div/div/div/div[1]/div[2]/span      ${CartPageTitle}
    IF    ${TitleCartPage} == True
        Input Text      id:first-name      ${FirstName}
        Input Text      id:last-name       ${LastName}
        Input Text      id:postal-code     ${PostalCode}
        Sleep           2s
        Click Button    id:continue
    ELSE
        Click Button    id:cancel
        Fail     Can't fill in blank input text 
        Close Browser 
    END
Check Checkout Overview
    ${CheckoutOverview}  Run Keyword And Return Status
    ...                Wait Until Element Contains    xpath:/html/body/div/div/div/div[1]/div[2]/span      ${CheckoutOverviewTitle}
    IF    ${CheckoutOverview} == True
        Wait Until Element Is Enabled      xpath:/html/body/div/div/div/div[2]/div/div[1]
        Wait Until Element Contains        xpath:/html/body/div/div/div/div[2]/div/div[2]/div[1]         ${PaymentInformation}
        Wait Until Element Contains      xpath:/html/body/div/div/div/div[2]/div/div[2]/div[3]           ${ShippingInformation}
        Wait Until Element Contains      xpath:/html/body/div/div/div/div[2]/div/div[2]/div[5]           ${PriceTotal}
        Sleep           2s
        Click Button    id:finish
    ELSE
        Click Button    id:cancel
        Fail     Can't access Checkout Overview 
        Close Browser 
    END
Check Checkout Complete
    ${CheckoutCompleteDes}  Run Keyword And Return Status
    ...                Wait Until Element Contains    xpath:/html/body/div/div/div/div[1]/div[2]/span      ${CheckoutComplete}
    IF    ${CheckoutCompleteDes} == True
        Wait Until Element Is Enabled      id:checkout_complete_container
        Sleep           2s
        Click Button    id:back-to-products
    ELSE
        Fail     Can't access Checkout Complete 
        Close Browser 
    END
Wait Error Message Wrong Password
    ${WaitErrorMessagePassword}  Run Keyword And Return Status
    ...                Wait Until Element Is Enabled    xpath:/html/body/div/div/div[2]/div[1]/div/div/form/div[3]
    IF    ${WaitErrorMessagePassword} == True
    Wait Until Element Contains      xpath:/html/body/div/div/div[2]/div[1]/div/div/form/div[3]/h3    ${ErrorMessage}
    ELSE
        Fail     No Error Showing 
        Close Browser 
    END
Wait Error Message Wrong Username
    ${WaitErrorMessageUsername}  Run Keyword And Return Status
    ...                Wait Until Element Is Enabled    xpath:/html/body/div/div/div[2]/div[1]/div/div/form/div[3]
    IF    ${WaitErrorMessageUsername} == True
    Wait Until Element Contains      xpath:/html/body/div/div/div[2]/div[1]/div/div/form/div[3]/h3    ${ErrorMessage}
    ELSE
        Fail     Can't access Checkout Complete 
        Close Browser 
    END

    
    
    
    
    
