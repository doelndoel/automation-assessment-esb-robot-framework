*** Variables ***
${Browser}                Chrome
${AppTitle}               Swag Labs
${Title}                  Products
${TitleCartPage}          Your Cart
${CartPageTitle}          Checkout: Your Information
${CheckoutOverviewTitle}  Checkout: Overview
${PaymentInformation}     Payment Information
${ShippingInformation}    Shipping Information
${PriceTotal}             Price Total
${CheckoutComplete}       Checkout: Complete!
${ErrorMessage}           Epic sadface: Username and password do not match any user in this service
${Delay}                  5s
${Delay2}                 3s
