*** Settings ***
Library    SeleniumLibrary
Library    Screenshot
Resource   Resources/GlobalKeywords.robot
*** Test Cases ***
#Positive Case
Login 
    Open Site
    Input Credential
    sleep    ${Delay2}
    Click Button Login
    sleep    ${Delay2}
    Wait Element Image
    Check Dashboard Title
    Check Title Product
    Check List Item
    Capture Page Screenshot
    sleep    ${Delay}
    [Teardown]    Close Browser
Success Checkout Item
    Open Site
    Input Credential
    sleep    ${Delay2}
    Click Button Login
    sleep    ${Delay2}
    Wait Element Image
    Check Dashboard Title
    Check Title Product
    Check List Item
    Click Add to cart
    Click Icon Cart
    sleep    ${Delay2}
    Cart Page Detail
    sleep    ${Delay2}
    Fill Detail Information
    sleep    ${Delay2}
    Check Checkout Overview
    Capture Page Screenshot
    Check Checkout Complete
    sleep    ${Delay}
    [Teardown]    Close Browser 
##Negative Case
Input Wrong Password 
    Open Site
    Input Wrong Password Credential
    sleep    ${Delay2}
    Click Button Login
    sleep    ${Delay2}
    Wait Error Message Wrong Password
    Capture Page Screenshot
    sleep    ${Delay}
    [Teardown]    Close Browser
Input Wrong Username
    Open Site
    Input Wrong Username Credential
    sleep    ${Delay2}
    Click Button Login
    sleep    ${Delay2}
    Wait Error Message Wrong Username
    Capture Page Screenshot
    sleep    ${Delay}
    [Teardown]    Close Browser